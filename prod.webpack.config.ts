import config from './webpack.config';

config.mode = 'production';

export default config;