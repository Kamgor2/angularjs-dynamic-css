import config from './webpack.config';

config.mode = 'development';
config.devtool = 'inline-source-map';

export default config;