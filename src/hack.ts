import * as angular from 'angular';

export const angularHack = () => {
    const listOfExistingStyles = new WeakMap();
    const originalModule = angular.module;
    const injector = angular.injector(['ng']);
    //tslint:disable
    // overwrite module registration function where we can get acces to component registration function
    (<any> angular).module = function () {
        const module = originalModule.apply(window, arguments);
        const originalComponent = module.component;
        // here is some magic part of code where we overwrite component onInit and onDestroy functions with 
        module.component = function(componentName, componentObject) {
            let $http = injector.get('$http');
            const originalOnInit = componentObject.controller.prototype.$onInit ? componentObject.controller.prototype.$onInit : () =>{};
            componentObject.controller.prototype.$onInit = function() {
                originalOnInit.apply(this, arguments);
                if(typeof componentObject.css === 'string' && componentObject.css) {
                    if(!listOfExistingStyles.has(componentObject)) {
                        $http.get(componentObject.css).then((response) => {
                            const style: HTMLStyleElement = document.createElement('style');
                            style.type = 'text/css';
                            style.textContent = <any> response.data;
                            listOfExistingStyles.set(componentObject, style);
                            document.querySelector('head').appendChild(style);
                        });
                    } else {
                        document.querySelector('head').appendChild(listOfExistingStyles.get(componentObject));
                    }
                }
            }

            const orginalOnDestroy = componentObject.controller.prototype.$onDestroy ? componentObject.controller.prototype.$onDestroy : () =>{};
            componentObject.controller.prototype.$onDestroy = function() {
                orginalOnDestroy.apply(this, arguments);
                if(listOfExistingStyles.has(componentObject)) {
                    listOfExistingStyles.get(componentObject).remove();
                }
            }

            const component = originalComponent.apply(this, arguments);
            return component;
        }
        return module;
    };
};