import * as angular from 'angular';
import { angularHack } from './hack';
import { CssServiceProvider } from './cssProvider';

export const angularCSS = angular
	.module('kamgor.dynamicCSS', [])
	.provider(CssServiceProvider.toString(), CssServiceProvider)
	.config(angularHack)
	.name;