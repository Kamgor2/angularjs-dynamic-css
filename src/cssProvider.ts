class CssService {

}

export class CssServiceProvider implements ng.IServiceProvider {

    private stylesPlacement: 'head' | 'body';

    public static toString() {
        return '$css';
    }

    public setStylesPlacement(placement: 'head' | 'body') {
        this.stylesPlacement = placement;
    }

    public getStylesPlacement() {
        return this.setStylesPlacement;
    }

    public $get(): CssService {
        return new CssService();
    }
}