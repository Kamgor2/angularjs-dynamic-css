import { Configuration } from "webpack";
import * as path from 'path';

const config: Configuration = {
    target: 'web',
    entry: {
        index: './src/index.ts'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader'
                    }
                ],
                exclude: [
                    /\.webpack.config.ts$/,
                    /node_modules/
                ]
            }
        ]
    },
    resolve: {
        extensions: [
            '.tsx',
            '.ts',
            '.js'
        ]
    },
    output: {
        path: path.resolve(__dirname, './dist/'),
        filename: 'index.js'
    }
};

export default config;